//
//  HelperFunctions.swift
//  JFruityNinja
//
//  Created by Jamal on 6/19/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation
import UIKit

func randomCGFloat(_ lowerLimit: CGFloat, _ upperLimit: CGFloat)-> CGFloat{
    return CGFloat(Float(arc4random()) / Float(UINT32_MAX)) * (upperLimit - lowerLimit) + lowerLimit
}
