//
//  Fruit.swift
//  JFruityNinja
//
//  Created by Jamal on 6/19/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import SpriteKit

class FruitNode: SKNode {
    
    let fruitEmojis = ["🍎", "🍇", "🍌", "🍉", "🍓", "🍅"]
    
    let bombEmoji = "💣"
    
    override init() {
        super.init()
        
        var emoji = ""
        
        if randomCGFloat(0, 1) < 0.9{
            name = "fruit"
            let n = Int(arc4random_uniform(UInt32(UInt(fruitEmojis.count))))
            emoji = fruitEmojis[n]
            
            //characters
            let ni = Int(randomCGFloat(0, 10000))
            let char = Unicode.Scalar(ni)
            emoji = String(char!)
        }else{
            name = "bomb"
            emoji = bombEmoji
        }
        
        let label = SKLabelNode(text: emoji)
        label.fontSize = 120
        label.verticalAlignmentMode = .center
        addChild(label)
        
        physicsBody = SKPhysicsBody()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
