//
//  GameScene.swift
//  JFruityNinja
//
//  Created by Jamal on 6/19/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import SpriteKit

enum GemePhase{
    case ready
    case inPlay
    case gameOver
}

class GameScene: SKScene {
    
    var gamePhase = GemePhase.ready
    var score = 0
    var best = 0
    var misses = 0
    var missesMax = 3
    
    var xMarks = XMarks()
    
    var promptLabel = SKLabelNode()
    var scoreLabel = SKLabelNode()
    var bestLabel = SKLabelNode()
    
    var fruitThrowTimer = Timer()
    
    var explodeOverlay = SKShapeNode()
    
    override func didMove(to view: SKView) {
        
        physicsWorld.gravity = CGVector(dx: 0, dy: -2)
        
        
        promptLabel = childNode(withName: "prompt label") as! SKLabelNode
        
        scoreLabel = childNode(withName: "score label") as! SKLabelNode
        scoreLabel.text = "\(score)"
        
        bestLabel = childNode(withName: "best label") as! SKLabelNode
        bestLabel.text = "\(best)"
        
        
        xMarks = XMarks(num: missesMax)
        xMarks.position = CGPoint(x: size.width - 60, y: size.height-60)
        addChild(xMarks)
        
        explodeOverlay = SKShapeNode(rect: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        explodeOverlay.fillColor = .white
        addChild(explodeOverlay)
        explodeOverlay.alpha = 0
        
        if UserDefaults.standard.object(forKey: "best") != nil {
            best = UserDefaults.standard.object(forKey: "best") as! Int
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if gamePhase == .ready{
            gamePhase = .inPlay
            startGame()
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches{
            
            let location = t.location(in: self)
            let previous = t.previousLocation(in: self)
            
            
            for node in nodes(at: location){
                
                if node.name == "fruit"{
                    score += 1
                    scoreLabel.text = "\(score)"
                    node.removeFromParent()
                    particleEffect(position: node.position)
                    playSoundEffect(soundFile: "ninja.m4a")
                }
                
                if node.name == "bomb"{
                    bombExplode()
                    gameOver()
                    particleEffect(position: node.position)
                }
                
                
                
            }
            
            let line = TrailLine(pos: location, lastPos: previous, width: 8, color: .blue)
            addChild(line)
            
            line.run(SKAction.sequence([
                SKAction.fadeAlpha(to: 0, duration: 0.2),
                SKAction.removeFromParent()
                
                ]))
            
        }
    }
    
    override func didSimulatePhysics() {
        
        for fruit in children {
            if fruit.position.y < -100{
                fruit.removeFromParent()
                if  fruit.name == "fruit"{
                    missFruit()
                }
            }
        }
        
    }
    
    func startGame(){
        
        misses = 0
        score = 0
        xMarks.reset()
        scoreLabel.text = "\(score)"
        
        bestLabel.text = "Best: \(best)"
        
        promptLabel.isHidden = true
        
        fruitThrowTimer = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: true){ _ in
            self.createFruits()
        }
    }
    
    func createFruits(){
        
        let numberOfFruits = 1 + Int(arc4random_uniform(UInt32(4)))
        
        for _ in 0..<numberOfFruits{
            
            let fruit = FruitNode()
            fruit.position.x = randomCGFloat(0, size.width)
            fruit.position.y = -100
            addChild(fruit)
            
            if fruit.position.x < size.width/2 {
                fruit.physicsBody?.velocity.dx = randomCGFloat(0, 200)
            }
            
            
            if fruit.position.x > size.width/2 {
                fruit.physicsBody?.velocity.dx = randomCGFloat(0, -200)
            }
            
            fruit.physicsBody?.velocity.dy = randomCGFloat(500, 800)
            fruit.physicsBody?.angularVelocity = randomCGFloat(-5, 5)
            
        }
        
        
    }
    
    func missFruit(){
        
        misses += 1
        
        xMarks.update(num: misses)
        
        if misses == missesMax{
            gameOver()
        }
    }
    
    func bombExplode(){
        
        for case let fruit as FruitNode in children{
            fruit.removeFromParent()
            particleEffect(position: fruit.position)
            
        }
        
        explodeOverlay.run(SKAction.sequence([
            SKAction.fadeAlpha(to: 1, duration: 0),
            SKAction.wait(forDuration: 0.2),
            SKAction.fadeAlpha(to: 0, duration: 0),
            SKAction.wait(forDuration: 0.2),
            SKAction.fadeAlpha(to: 1, duration: 0),
            SKAction.wait(forDuration: 0.2),
            SKAction.fadeAlpha(to: 0, duration: 0)
            ]))
    }
    
    func gameOver(){
        if score > best{
            best = score
            
            UserDefaults.standard.set(best, forKey: "best")
            UserDefaults.standard.synchronize()
        }
        
        promptLabel.isHidden = false
        promptLabel.text = "Game Over"
        promptLabel.setScale(0)
        promptLabel.run(SKAction.scale(to: 1, duration: 0.3))
        
        gamePhase = .gameOver
        
        fruitThrowTimer.invalidate()
        
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false){_ in
            self.gamePhase = .ready
        }
        
    }
    
    func particleEffect(position: CGPoint){
        let emitter = SKEmitterNode(fileNamed: "Explode.sks")
        emitter?.position = position
        addChild(emitter!)
    }
    
    func playSoundEffect(soundFile: String){
        let audioNode = SKAudioNode(fileNamed: soundFile)
        audioNode.autoplayLooped = false
        addChild(audioNode)
        audioNode.run(SKAction.play())
        audioNode.run(SKAction.sequence([
            SKAction.wait(forDuration: 1.0),
            SKAction.removeFromParent()
            ]))
    }
    
}
