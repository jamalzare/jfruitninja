//
//  XMarks.swift
//  JFruityNinja
//
//  Created by Jamal on 6/19/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import SpriteKit

class XMarks: SKNode{
    
    var xArray = [SKSpriteNode]()
    var numXs = Int()
    
    let blackXpic = SKTexture(imageNamed: "black x")
    let redXpic = SKTexture(imageNamed: "red x")
    
    init(num: Int = 0) {
        super.init()
        
        numXs = num
        
        for i in 0..<num{
            let xMark = SKSpriteNode(imageNamed: "black x")
            xMark.size = CGSize(width: 60, height: 60)
            xMark.position.x = -CGFloat(i) * 70
            addChild(xMark)
            xArray.append(xMark)
        }
    }
    
    func update(num: Int){
        if num <= numXs{
            xArray[xArray.count - num].texture = redXpic
        }
    }
    
    func reset(){
        for xMark in xArray{
            xMark.texture = blackXpic
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
